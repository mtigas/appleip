# appleip

a tiny python script to test IPs against the Apple iCloud Private Relay ip list.

© 2021 Mike Tigas. Licensed under the [GNU Affero General Public License v3 or later](LICENSE.txt), aka "AGPL-3.0-or-later". (Also available [here](https://spdx.org/licenses/AGPL-3.0-or-later.html).)

---

init:

```bash
virtualenv -p `which python3.9` .
source bin/activate
pip install -UI pip
pip install -r requirements.txt
```

example use:

```bash
$ ./ip.py 104.28.103.14
'checking IP 104.28.103.14'

{'city': 'Chicago',
 'country': 'US',
 'extra': '',
 'net_range': '104.28.103.14/32',
 'region': 'US-IL'}

$ ./ip.py '2606:54c0:7680:11e8::e:15d'
'checking IP 2606:54c0:7680:11e8::e:15d'

{'city': 'Chicago',
 'country': 'US',
 'extra': '',
 'net_range': '2606:54c0:7680:11e8::/64',
 'region': 'US-IL'}
```