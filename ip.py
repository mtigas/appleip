#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
#
# appleip - https://0xacab.org/mtigas/appleip
# Copyright © 2021, Mike Tigas.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ipaddress
import argparse
import csv
import os.path
from pprint import pprint

import requests

DATAFILE_URL = "https://mask-api.icloud.com/egress-ip-ranges.csv"
DATAFILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "egress-ip-ranges.csv"
)
if __name__ == "__main__":
    # https://docs.python.org/3.9/library/argparse.html

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('ip_addr', type=str, nargs=1,
                        help='an IPv4 or IPv6 address')

    args = parser.parse_args()

    did_match = False

    # https://docs.python.org/3.9/library/ipaddress.html
    input_ip_addr = ipaddress.ip_address(args.ip_addr[0])
    pprint(f"checking IP {input_ip_addr}")
    print()

    # https://developer.apple.com/support/prepare-your-network-for-icloud-private-relay
    if not os.path.exists(DATAFILE):
        print(f"Downloading egress-ip-ranges.csv from {DATAFILE_URL}")
        range_csv_response = requests.get(DATAFILE_URL)
        with open(DATAFILE, 'wb') as fd:
            for chunk in range_csv_response.iter_content(chunk_size=128):
                fd.write(chunk)
    else:
        print(f"Using cached egress-ip-ranges.csv. Delete if you want to re-fetch from Apple.")


    with open(DATAFILE, 'r') as csvfile:
        range_csv = csv.DictReader(
            csvfile,
            fieldnames=(
                'net_range',
                'country',
                'region',
                'city',
                'extra'
            )
        )
    
        for row in range_csv:
            # https://docs.python.org/3.9/library/ipaddress.html
            net_range = ipaddress.ip_network(row['net_range'])
            # don't even bother testing if one is IPv4 and the other is IPv6
            if net_range.version != input_ip_addr.version:
                continue
            if input_ip_addr in net_range:
                pprint(row)
                print()
                did_match = True
                # don't break here, in case there's multiple matches

    if not did_match:
        print("No matches")
        print()
